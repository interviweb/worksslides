<?php
const TEST_SLIDE = [[
    0, 0, '/images/slide.svg',
    'Тестовый слайд', 'Это тестовый слайд. Его нужно отредактировать или удалить.',
    'Всплывающий заголовок', 'А это всплывающий текст слайда.',
    '<a href="/" target="_blank">Ссылка</a>'
]];
const EXAMPLE_SLIDE = [[
    'id' => -1,
    'page' => -1,
    'pos' => -1,
    'image' => '/images/add.svg',
    'title_back' => 'Задний заголовок',
    'text_back' => 'Задний текст.',
    'title_top' => 'Передний заголовок',
    'text_top' => 'Передний текст.',
    'links' => '<a href="/" target="_blank">Ссылка</a>'
]];

require_once __DIR__ . '/config.php';

class Auth {
    public function __construct() {
        session_set_cookie_params(LIMIT, PATH, DOMAIN, HTTPS, HTTPONLY);
        session_name(SESSION_NAME);
        if (!session_start()) {
            die('Session not started.');
        }
    }

    public function get_csrf() {
        $result = filter_input(INPUT_SESSION, 'csrf');
        if (empty($result)) {
            $result = hash('sha256', bin2hex(random_bytes(128)));
            $_SESSION['csrf'] = $result;
        }
        return $result;
    }

    public function is_valid_csrf(string $csrf): bool {
        if (!isset($_SESSION['csrf'])) return false;
        return strcmp($_SESSION['csrf'], $csrf) === 0;
    }

    public function is_login(): bool {
        if (!isset($_SESSION['valid']) || !$_SESSION['valid']) return false;
        if (strcmp($_SESSION['agent'], $_SERVER['HTTP_USER_AGENT']) !== 0) return false;
        if (strcmp($_SESSION['ip'], $_SERVER['REMOTE_ADDR']) !== 0) return false;
    }

    public function auth(string $passwd): bool {
        $hashpass = hash('sha256', hash('sha256', $passwd) . SALT);
        if (strcmp($hashpass, HASHPASS) !== 0) return false;
        $_SESSION['agent'] = $_SERVER['HTTP_USER_AGENT'];
        $_SESSION['ip'] = $_SERVER['REMOTE_ADDR'];
        $_SESSION['valid'] = true;
        return true;
    }

    public function logout() {
        $_SESSION['valid'] = false;
    }

    public function change_pass(string $passwd): bool {
        $path = __DIR__ . '/config.php';
        if (!is_readable($path) || !is_writable($path)) return false;
        $pwd = hash('sha256', hash('sha256', $passwd) . SALT);
        $result = file_put_contents($path, str_replace(HASHPASS, $pwd, file_get_contents($path)));
        if ($result) return true;
        return false;
    }
}

class TemplateBuilder {
    private $index = '';
    private $slide = '';

    public function __construct() {
        $this->index = file_get_contents(__DIR__ . '/template/index.html');
        $this->slide = file_get_contents(__DIR__ . '/template/slide.html');
    }

    public function get_slides(array $data): array {
        $result = [];
        foreach($data as $field) {
            $result[] = str_replace(
                [
                    '{id}', '{pos}', '{image}', '{title_back}',
                    '{text_back}', '{title_top}', '{text_top}', '{links}'
                ],
                [
                    $field['id'], $field['pos'], $field['image'], $field['title_back'],
                    $field['text_back'], $field['title_top'], $field['text_top'], $field['links']
                ],
                $this->slide
            );
        }
        return $result;
    }

    public static function make_block(array $slides): string {
        $result = '';
        foreach($slides as $slide) {
            $result .= $slide;
        }
        return $result;
    }

    public function get_index(array $data, int $page, int $total, int $size, string $csrf): string {
        return str_replace(
            ['{content}', '{page}', '{total}', '{size}', '{csrf}'],
            [$this->make_block($this->get_slides($data)), $page, $total, $size, $csrf],
            $this->index
        );
    }
}

class DataBase {
    private $db;

    public function __construct(bool $readonly = false) {
        $path = __DIR__ . '/sqlite.db';
        if (!is_file($path)) {
            $this->db = new SQLite3($path);
            $this->db->exec(
                'CREATE TABLE IF NOT EXISTS slides ('
                . 'id integer primary key autoincrement, '
                . 'page integer not null, '
                . 'pos integet not null, '
                . 'image text, '
                . 'title_top text, '
                . 'text_top text, '
                . 'title_back text, '
                . 'text_back text, '
                . 'links text'
                . ');'
            );
            $this->add_slides(TEST_SLIDE);
        } elseif ($readonly) {
            $this->db = new SQLite3($path, SQLITE3_OPEN_READONLY);
        } else {
            $this->db = new SQLite3($path);
        }
    }

    public function get_block(int $page): array {
        $result = [];
        $query = $this->db->query("SELECT * FROM `slides` WHERE `page`='$page' ORDER BY `pos`;");
        if (empty($query)) return result;
        while($slide = $query->fetchArray(SQLITE3_ASSOC)) {
            if ($slide) {
                $result[] = $slide;
            }
        }
        return $result;
    }

    public function add_slides(array $data) {
        $values = '';
        foreach($data as $slide) {
            $values .= '(';
            foreach($slide as $field) {
                $values .= "'" . SQLite3::escapeString($field) . "', ";
            }
            $values = substr($values, 0, strlen($values) - 2) . '), ';
        }
        $values = substr($values, 0, strlen($values) - 2);
        $fields = "(`page`, `pos`, `image`, `title_top`, `text_top`, `title_back`, `text_back`, `links`)";
        $this->db->exec("INSERT OR IGNORE into `slides` " . $fields . " VALUES " . $values);
    }

    public function edit_slides(array $data) {
        foreach($data as $slide) {
            $set = '';
            foreach($slide as $key => $value) {
                if ($key == 'id') continue;
                $val = SQLite3::escapeString($value);
                $set .= "`$key`='$val', ";
            }
            $set = substr($set, 0, strlen($set) - 2);
            $id = $slide['id'];
            $this->db->exec("UPDATE `slides` SET $set WHERE `id`='$id';");
        }
    }

    public function sort(int $len) {
        $oldBlock = null;
        for ($i = 0; $i < $this->size(); $i++) {
            $block = $this->get_block($i);
            if ($oldBlock == null) {
                $oldBlock = $block;
                continue;
            }
            while (count($oldBlock) < $len) {
                $oldBlock[] = array_shift($block);
            }
            $this->edit_slides($oldBlock);
            $oldBlock = $block;
        }
    }

    public function size(): int {
        $query = $this->db->query("SELECT COUNT(*) as count FROM `slides`;");
        return $query->fetchArray(SQLITE3_ASSOC)['count'];
    }

    public function pages(): int {
        $query = $this->db->query("SELECT MAX(`page`) FROM `slides`;");
        if (empty($query)) return 0;
        return $query->fetchArray(SQLITE3_ASSOC)['MAX(`page`)'];
    }

    public function close() {
        $this->db->close();
    }
}

function upload() {
    $path = hash('md5', bin2hex(random_bytes(128)));
    $path = '/files/' . $path . '.' . strtolower(pathinfo($_FILES['file']['name'])['extension']);
    $uploaded = move_uploaded_file($_FILES['file']['tmp_name'], __DIR__ . $path);
    if ($uploaded) {
        die($path);
    } else {
        http_response_code($_FILES['userfile']['error']);
        die($_FILES['userfile']['error']);
    }
}

function get_data(): array {
    $data = file_get_contents('php://input');
    if (empty($data)) {
        http_response_code(400);
        die('400 Bad Request');
    }
    return json_decode($data, true);
}

function post(Auth $auth, array $data) {
    switch($data['action']) {
        case 'edit':
            if (!$auth->is_login()) {
                http_response_code(401);
                die('401 Unauthorized');
            }
            $db = new DataBase();
            $db->edit_slides(json_decode($data['slides'], true));
            $db->close();
            break;
        case 'sort':
            $db = new DataBase();
            $db->sort($data['len']);
            $db->close();
            break;
        case 'auth':
            if ($auth->auth($data['password'])) {
                break;
            } else {
                http_response_code(401);
                die('401 Unauthorized');
            }
            break;
        case 'change_password':
            if ($auth->auth($data['password']) && $auth->change_pass($data['newpassword'])) {
                break;
            } else {
                http_response_code(401);
                die('401 Unauthorized');
            }
            break;
        case 'logout':
            $auth->logout();
            break;
        default:
            http_response_code(400);
            die('400 Bad Request');
    }
}

function get_page(string $url, int $limit): int {
    $result = 0;
    if (preg_match('/^\/page\/\d$/', $url) === 1) {
        $result = intval(substr($url, strrpos($url, '/') + 1));
    }
    if ($result < 0) $result = 0;
    if ($result > $limit) $result = $limit;
    return $result;
}

function echo_example() {
    header('Content-Type: application/json');
    echo json_encode((new TemplateBuilder())->get_slides(EXAMPLE_SLIDE));
}

function echo_json(string $url) {
    $db = new DataBase(true);
    $page = get_page($url, $db->pages());
    $block = $db->get_block($page);
    $db->close();
    header('Content-Type: application/json');
    echo json_encode((new TemplateBuilder())->get_slides($block));
}

function echo_html(string $url, string $csrf) {
    $db = new DataBase(true);
    $total = $db->pages();
    $page = get_page($url, $total);
    $size = $db->size();
    $block = $db->get_block($page);
    $db->close();
    echo (new TemplateBuilder())->get_index($block, $page + 1, $total + 1, $size, $csrf);
}

function send_file(string $url) {
    $path = __DIR__ . '/template' . $url;
    if (preg_match('/^\/files/', $url)) {
        $path = __DIR__ . $url;
    }
    if (!is_file($path)) {
        http_response_code(404);
        die('404 Not Found');
    }
    $mime = mime_content_type($path);
    switch(strtolower(pathinfo($path)['extension'])) {
        case 'css':
            $mime = 'text/css';
            break;
        case 'js':
            $mime = 'text/javascript';
            break;
        case 'svg':
            $mime = 'image/svg+xml';
            break;
    }
    header('Content-Description: File Transfer');
    header('Content-Type: ' . $mime);
    header('Content-Disposition: attachment; filename="' . basename($path) . '"');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($path));
    readfile($path);
}

$auth = new Auth();
$method = filter_input(INPUT_SERVER, 'REQUEST_METHOD');

switch ($method) {
    case 'POST':
        $csrf = filter_input(INPUT_POST, 'csrf');
        if (!empty($csrf)) {
            if (!$auth->is_valid_csrf($csrf)) {
                http_response_code(401);
                die('401 Unauthorized');
            }
            upload();
        }
        $data = get_data();
        if (!$auth->is_valid_csrf($data['csrf'])) {
            http_response_code(401);
            die('401 Unauthorized');
        }
        post($auth, $data);
        break;
    case 'GET':
        $url = filter_input(INPUT_GET, 'url');
        if (empty($url)) {
            $url = filter_input(INPUT_SERVER, 'REQUEST_URI');
        }
        if (preg_match('/^(\/)$|^(\/index\.php)$|^(\/page\/\d)$/', $url) === 1) {
            if (!empty(filter_input(INPUT_GET, 'example'))) {
                echo_example();
                break;
            }
            if (!empty(filter_input(INPUT_GET, 'json'))) {
                echo_json($url);
                break;
            }
            echo_html($url, $auth->get_csrf());
        } else {
            send_file($url);
        }
        break;
    default:
        http_response_code(400);
        die('400 Bad Request');
}
