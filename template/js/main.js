const LIMIT = 6;
let drake = null;
let slideClicked = null;
let linkClicked = null;

function toggleLogin() {
    $('#login').toggle('slow');
}

function addSlide() {
    if ($('#content .slide').length >= LIMIT) return;
    requestData('/?url=/&example=true', function(data) {
        let block = JSON.parse(data)[0];
        let element = $($.parseHTML(block)).addClass('add-block').attr('onclick', 'slideClick($(this))');
        setHover(element);
        $('#mirror-container').prepend(element);
    });
}

function save() {

}

function cancel() {

}

function back() {

}

function go() {

}

function login() {
    let passwd = $('#ipass').val();
    if (passwd.length === 0) {
        $('#login-error').html('Введите пароль!');
        $('#login-error').show();
        return;
    }
    let csrf = $('#csrf').val();
    let data = {'password': passwd, 'csrf': csrf, 'action': 'auth'};
    let request = new XMLHttpRequest();
    request.open('POST', '/', true);
    request.setRequestHeader('Content-Type', 'application/json');
    request.send(JSON.stringify(data));
    request.onreadystatechange = function() {
        if (request.readyState !== 4) return;
        if (request.status === 200) {
            setCookie('authed', true);
            $('#login').toggle('slow');
            $('#login-error').hide();
            $('#ipass').val('');
            toggleManage(true);
        } else {
            $('#login-error').html('Неверный пароль!');
            $('#login-error').show();
        }
    };
}

function changePassword() {
    let passwd = $('#ipass').val();
    let newpasswd = $('#newpass').val();
    if (passwd.length === 0 || newpasswd.length === 0) {
        $('#login-error').html('Введите пароль!');
        $('#login-error').show();
        return;
    }
    let csrf = document.getElementById('csrf').value;
    let data = {'password': passwd, 'newpassword': newpasswd, 'csrf': csrf, 'action': 'change_password'};
    let request = new XMLHttpRequest();
    request.open('POST', '/', true);
    request.setRequestHeader('Content-Type', 'application/json');
    request.send(JSON.stringify(data));
    request.onreadystatechange = function() {
        if (request.readyState !== 4) return;
        if (request.status === 200) {
            setCookie('authed', true);
            $('#login').toggle('slow');
            $('#login-error').hide();
            $('#ipass').val('');
            $('#newpass').val('');
            toggleManage(true);
        } else {
            $('#login-error').html('Неверный пароль!');
            $('#login-error').show();
        }
    };
}

function logout() {
    let csrf = $('#csrf').val();
    let data = {'csrf': csrf, 'action': 'logout'};
    let request = new XMLHttpRequest();
    request.open('POST', '/', true);
    request.setRequestHeader('Content-Type', 'application/json');
    request.send(JSON.stringify(data));
    setCookie('authed', false);
    $('#ipass').val('');
    $('#newpass').val('');
    toggleManage(false);
    $('#login-error').hide();
    $('#login').toggle('slow');
}

function requestData(url, func) {
    let request = new XMLHttpRequest();
    request.open('GET', url, true);
    request.send();
    request.onreadystatechange = function() {
        if (request.readyState !== 4) return;
        if (request.status === 200) {
            func(request.responseText);
        }
    };
}

function toggleManage(enabled) {
    if (enabled) {
        let page = Number($('#page-num'));
        let total = Number($('#page-total'));
        if (page > 1) {
            requestData('?url=/page/' + (page - 1) + '&json=true', function(data) {
                let block = '';
                for (let i = data.length, n = 0; n < 3; i--, n++) {
                    block = block + data[i];
                }
                $('#slides-left').html(block);
                $('#slides-left').show();
            });
        }
        if (total - page < total) {
            requestData('?url=/page/' + (page + 1) + '&json=true', function(data) {
                let block = '';
                for (let i = 0; i < 3; i++) {
                    block += data[i];
                }
                $('#slides-right').html(block);
                $('#slides-right').show();
            });
        } else {
            $('#slides-right').show();
        }
        drake = dragula(
            [
                document.getElementById('content'),
                document.getElementById('slides-left'),
                document.getElementById('slides-right'),
                document.getElementById('mirror-container')
            ],
            {'removeOnSpill': true}
        );
        drake.on('drop', function(el, target) {
            let cont = $(target);
            if (cont.hasClass('more-slides') && cont.children('.slide').length > 2) {
                drake.cancel(true);
            } else if (cont.attr('id') === 'content' && cont.children('.slide').length > LIMIT) {
                drake.cancel(true);
            } else if (cont.attr('id') === 'mirror-container' && cont.children('.slide').length > 1) {
                drake.cancel(true);
            }
        });
        $('#mirror-container').show();
        $('.login-hide').hide('slow');
        $('#manage').show('slow');
        $('.slide').attr('onclick', 'slideClick($(this))');
    } else {
        $('.slide').removeAttr('onclick');
        $('.add-block').removeClass('add-block');
        $('.more-slides').html('');
        $('.more-slides').hide();
        $('#mirror-container').html('');
        $('#mirror-container').hide();
        if (drake !== null) drake.destroy();
        $('.login-hide').show('slow');
        $('#manage').hide('slow');
    }
}

function getCookie(name) {
    let matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}

function setCookie(name, value) {
    document.cookie = name + '=' + value + ';';
}

function slideClick(slide) {
    if ($('#edit-slide-space').is(':visible')) return;
    slideClicked = slide;
    let html = slide.html();
    let left = '<div class="slide left-slide">' + html + '</div>';
    let add = '<a href="javascript:void(0);" onclick="addLink()" id="add-link">Добавить ссылку</a>';
    let right = '<div class="slide right-slide">' + html + add + '</div>';
    $('#edit-slide-space').html(left + right);
    $('#edit-slide').show('slow');
    $('#edit-slide .left-slide .slide-top .title-top').hover(function() {
        let text = $(this).html();
        $(this).html('<input type="text" id="in-text" class="input-links" value="' + text + '"/>');
    }, function() {
        $(this).html($('#in-text').val());
    });
    $('#edit-slide .left-slide .slide-top .text-top').hover(function() {
        let text = $(this).html();
        $(this).html('<textarea id="in-text" class="input-links text-area">' + text + '</textarea>');
    }, function() {
        $(this).html($('#in-text').val());
    });
    $('#edit-slide .right-slide .slide-back .title-back').hover(function() {
        let text = $(this).html();
        $(this).html('<input type="text" id="in-text" class="input-links" value="' + text + '"/>');
    }, function() {
        $(this).html($('#in-text').val());
    });
    $('#edit-slide .right-slide .slide-back .text-back').hover(function() {
        let text = $(this).html();
        $(this).html('<textarea id="in-text" class="input-links text-area">' + text + '</textarea>');
    }, function() {
        $(this).html($('#in-text').val());
    });
    setClick($('#edit-slide .right-slide .slide-back .slide-links a'));
}

function saveSlide() {
    slideClicked.children('.slide-top').children('.title-top').html(
        $('#edit-slide .left-slide .slide-top .title-top').html()
    );
    slideClicked.children('.slide-top').children('.text-top').html(
        $('#edit-slide .left-slide .slide-top .text-top').html()
    );
    slideClicked.children('.slide-back').children('.title-back').html(
        $('#edit-slide .right-slide .slide-back .title-back').html()
    );
    slideClicked.children('.slide-back').children('.text-back').html(
        $('#edit-slide .right-slide .slide-back .text-back').html()
    );
    slideClicked.children('.slide-back').children('.slide-links').html(
        $('#edit-slide .right-slide .slide-back .slide-links').html()
    );
    slideClicked.children('.slide-top').css(
        'background-image',
        $('#edit-slide .left-slide .slide-top').css('background-image')
    );
    cancelSlide();
}

function cancelSlide() {
    $('#edit-slide').hide('slow', function() {
        $('#edit-slide-space').html('');
        $('#file-input').val('');
    });
}

function loadBg() {
    let form = new FormData();
    let file = $('#file-input')[0].files[0];
    form.append('file', file);
    form.append('csrf', $('#csrf').val());
    let request = new XMLHttpRequest();
    request.open('POST', '/', true);
    request.send(form);
    request.onreadystatechange = function() {
        if (request.readyState !== 4) return;
        if (request.status === 200) {
            $('#edit-slide .left-slide .slide-top').css(
                'background-image', 'url(' + request.responseText + ')'
            );
        }
    };
}

function setHover(element) {
    element.hover(function() {
        element.children('.slide-top').stop().animate({'opacity': 0}, 200, 'linear', function() {
        element.children('.slide-top').css('display', 'none');
    });
    }, function() {
        element.children('.slide-top').css('display', 'block');
        element.children('.slide-top').stop().animate({'opacity': 1}, 200, 'linear');
    });
}

function setClick(element) {
    element.click(function() {
        if ($('#link-edit').is(':visible')) return false;
        $('#in-link').val($(this).attr('href'));
        $('#in-text').val($(this).html());
        $('#link-edit').show('fast');
        linkClicked = $(this);
        return false;
    });
}

function saveLink() {
    let link = $('#in-link').val();
    let text = $('#in-text').val();
    if (link.length > 0 && text.length > 0) {
        linkClicked.attr('href', link);
        linkClicked.html(text);
        setClick(linkClicked);
    } else {
        linkClicked.remove();
    }
    cancelLink();
}

function cancelLink() {
    if ($('#in-link').val().length === 0 || $('#in-text').val().length === 0) {
        linkClicked.remove();
    }
    $('#in-link').val('');
    $('#in-text').val('');
    $('#link-edit').hide('fast');
    linkClicked = null;
}

function removeLink() {
    linkClicked.remove();
    cancelLink();
}

function addLink() {
    if ($('#link-edit').is(':visible')) return;
    $('#edit-slide .right-slide .slide-back .slide-links').append(
        '<a href="" target="_blank"></a>'
    );
    linkClicked = $('#edit-slide .right-slide .slide-back .slide-links a').last();
    $('#link-edit').show('fast');
}

if (getCookie('authed') === 'true') {
    toggleManage(true);
}

setHover($('.slide, .add-block'));

$('#file-input').change(function() {
   loadBg();
});
